module chainmaker.org/chainmaker/tcip-bcos/v2

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.3.1
	chainmaker.org/chainmaker/sdk-go/v2 v2.3.1
	chainmaker.org/chainmaker/tcip-go/v2 v2.3.1
	github.com/FISCO-BCOS/go-sdk v1.0.0
	github.com/Knetic/govaluate v3.0.1-0.20171022003610-9aa49832a739+incompatible
	github.com/cloudflare/cfssl v1.6.1
	github.com/common-nighthawk/go-figure v0.0.0-20210622060536-734e95fb86be
	github.com/emirpasic/gods v1.18.1
	github.com/ethereum/go-ethereum v1.10.4
	github.com/gogo/protobuf v1.3.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/spf13/cobra v1.4.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.10.1
	github.com/stretchr/testify v1.7.0
	github.com/syndtr/goleveldb v1.0.1-0.20210305035536-64b5b1c73954
	github.com/tmc/grpc-websocket-proxy v0.0.0-20201229170055-e5319fda7802
	go.uber.org/zap v1.21.0
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d
	google.golang.org/grpc v1.43.0
	google.golang.org/protobuf v1.27.1
)
